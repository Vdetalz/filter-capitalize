<?php

namespace Drupal\filter_capitalize\Plugin\Filter;

use Drupal\filter\FilterProcessResult;
use Drupal\filter\Plugin\FilterBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * @Filter(
 *   id = "filter_capitalize",
 *   title = @Translation("Auto Capitalize Filter Settings"),
 *   description = @Translation("Auto­capitalizes pre­configured words anywhere they occur in the filtered text."),
 *   type = Drupal\filter\Plugin\FilterInterface::TYPE_MARKUP_LANGUAGE,
 * )
 */
class FilterCapitalize extends FilterBase {

  /**
   * Words for replace.
   *
   * @var array
   */
  private $replaceWords = [];

  /**
   * Words needs to capitalize.
   *
   * @var array
   */
  private $searchWords = [];

  /**
   * {@inheritdoc}
   */
  public function process($text, $langcode) {
    if ($this->isSearchWords()) {
      $this->setSearchWords();
      $this->setReplaceWords($this->searchWords);
      $text = str_replace($this->searchWords, $this->replaceWords, $text);
      //return new FilterProcessResult($new_text);
    }
    return new FilterProcessResult($text);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form['filter_capitalize'] = array(
      '#type' => 'textarea',
      '#title' => $this->t('Words to capitalize'),
      '#default_value' => isset($this->settings['filter_capitalize']) ? $this->settings['filter_capitalize'] : '',
      '#description' => $this->t('Set the list of words to capitalize. Use comma(,) to separate them. For Example: test, word, simple'),
    );
    return $form;
  }

  /**
   * Sets the array of the $searchWords.
   */
  private function setSearchWords() {
    $string = mb_strtolower($this->settings['filter_capitalize'], 'utf-8');
    $string = trim($string);
    $string = str_replace(' ', '', $string);
    $this->searchWords = explode(',', $string);
/*    foreach ($words as $word) {
      $this->searchWords[] = trim($word);
    }*/
  }

  /**
   * Sets the array of the $replaceWords.
   *
   * @param array $words
   *   Search words.
   */
  private function setReplaceWords(array $words) {
    foreach ($words as $word) {
      $this->replaceWords[] = $this->filterMbUcfirst(trim($word));
    }
  }

  /**
   * Multilingual ucfirst.
   *
   * @param string $str
   *
   * @return string
   */
  private function filterMbUcfirst($str) {
    $fc = mb_strtoupper(mb_substr($str, 0, 1));
    return $fc . mb_substr($str, 1);
  }

  /**
   * Check is empty $searchWords.
   *
   * @return bool
   *   Returns FALSE if empty and TRUE otherwise.
   */
  private function isSearchWords() {
    $state = empty($this->settings['filter_capitalize']) ? FALSE : TRUE;
    return $state;
  }

}
